﻿export interface IHashMap<TValue> {
    [key: string]: TValue;
} 