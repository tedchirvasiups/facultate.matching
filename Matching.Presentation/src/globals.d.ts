﻿declare var __webpack_public_path__: string; 

//Permite incarcarea fisierelor prin import
//https://github.com/Microsoft/TypeScript-React-Starter/issues/12
declare module '*.png';

interface Window {
    baseUrl: string;
    resourcesBaseUrl: string;
    appVersion: string;
}