﻿import { Node } from './Node'
import { IHashMap } from 'src/data-structures/IHashMap';
import { IEdge } from './IEdge';

export interface NodeCollection extends IHashMap<Node> { }
export interface EdgeCollection extends IHashMap<IEdge[]> { }

export class Graph {

    Nodes: NodeCollection = {};
    Edges: EdgeCollection = {};

    addEdge = (edge: IEdge) => {
        let edgesCollection = this.Edges[edge.from.id];

        if (edgesCollection == null) {
            edgesCollection = this.Edges[edge.from.id] = [];
        }

        edgesCollection.push(edge);
    }

    addNode = (node: Node) => {
        this.Nodes[node.id] = node;
    }

}