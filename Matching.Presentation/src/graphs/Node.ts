﻿export class Node {

    id: string;

    x: number = 0;
    y: number = 0;

    color: string;

    copy = (): Node  => {

        let node = new Node();
        node.id = this.id;
        node.x = this.x;
        node.y = this.y;
        node.color = this.color;

        return node;
    }

}