﻿import { Node } from './Node'

export interface IEdge {
    from?: Node;
    to: Node;
    color?: string;
    capacity?: number;
}