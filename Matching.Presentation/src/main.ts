﻿import './main.css'
import * as p5 from 'p5'
import { Graph, NodeCollection } from './graphs/Graph';
import { randomRangeInt } from './randomRangeInt';
import { Node } from './graphs/Node';
import { IEdge } from './graphs/IEdge';
import { BipartiteGraph } from './graphs/BipartiteGraph';

const sketch = (p: p5) => {

    let graph: Graph;
    const nodesMinCount = 5;
    const nodesMaxCount = 20;

    const createRandomBipartiteGraph = ({ spacing }: { spacing: number }) => {

        let bipartiteGraph: BipartiteGraph = graph = new BipartiteGraph();

        const graphSize = randomRangeInt(nodesMinCount, nodesMaxCount);
        const setASize = Math.floor(graphSize / 2);
        const setBSize = graphSize - setASize;

        let idCounter = 0;
        const setANodes: NodeCollection = {};
        const setBNodes: NodeCollection = {};

        //Create set A nodes
        for (let i = 0; i < setASize; i++) {

            let node = new Node();
            node.id = idCounter.toString();
            node.color = "#673AB7"; //Deep purple
            node.x = -150;
            node.y = i * spacing;

            setANodes[node.id] = node;
            graph.addNode(node);

            idCounter++;
        }

        //Create set B nodes
        for (let i = 0; i < setBSize; i++) {

            let node = new Node();
            node.id = idCounter.toString();
            node.color = "#F44336"; //Red
            node.x = 150;
            node.y = i * spacing;

            setBNodes[node.id] = node;
            graph.addNode(node);

            idCounter++;
        }

        let setBIds = Object.keys(setBNodes);

        //Create edges
        for (let nodeAId in setANodes) {

            let nodeA = setANodes[nodeAId];
            let edgesCount = randomRangeInt(0, setBSize);

            for (let j = 0; j < edgesCount; j++) {

                let nodeB = setBNodes[setBIds[j]];
                let edge: IEdge = {
                    from: nodeA,
                    to: nodeB,
                    capacity: 1,
                    color: "#009688" //Teal
                };

                graph.addEdge(edge);
            }
        }

        bipartiteGraph.setA = setANodes;
        bipartiteGraph.setB = setBNodes;

    };

    p.setup = () => {
        p.createCanvas(p.windowWidth, p.windowHeight);

        createRandomBipartiteGraph({ spacing: 80 });
    }

    p.draw = () => {

        p.background("rgb(16, 16, 16)");
        const offset = { x: p.windowWidth / 2, y: 100 };

        const drawNode = (node: Node, offset: { x: number, y: number }) => {
            p.fill(node.color);
            p.noStroke();
            let x = node.x + offset.x;
            let y = node.y + offset.y;
            p.circle(x, y, 20);
            p.fill("white");
            p.textAlign(p.CENTER, p.CENTER)
            p.text(node.id, x, y);
        }

        const drawEdge = (edge: IEdge, offset: { x: number, y: number }) => {

            p.stroke(edge.color);

            let xA = edge.from.x + offset.x;
            let yA = edge.from.y + offset.y;
            let xB = edge.to.x + offset.x;
            let yB = edge.to.y + offset.y;

            p.line(xA, yA, xB, yB);
        }

        //Draw edges
        for (let nodeId in graph.Nodes) {
            let node = graph.Nodes[nodeId];
            let nodeEdges = graph.Edges[node.id];

            if (nodeEdges != null) {

                for (let edge of nodeEdges) {
                    drawEdge(edge, offset);
                }
            }
        }

        //Draw nodes
        for (let nodeId in graph.Nodes) {
            let node = graph.Nodes[nodeId];
            drawNode(node, offset);
        }
    }
}

new p5(sketch);