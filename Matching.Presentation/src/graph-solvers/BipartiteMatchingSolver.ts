﻿import { BipartiteGraph } from "src/graphs/BipartiteGraph";
import { Node } from "../graphs/Node"
import { IEdge } from "src/graphs/IEdge";

export class BipartiteMatchingSolver {

    visitedToken: number = 0;
    visitedNodes: string[] = [];
    maxFlow: number = 0;

    source: Node;
    sink: Node;

    graph: BipartiteGraph;

    constructor(graph: BipartiteGraph) {

        //Copy graph
        let bpGraph = new BipartiteGraph();

        bpGraph.Nodes = Object.assign({}, graph.Nodes);
        bpGraph.Edges = Object.assign({}, graph.Edges);

        //Create source
        this.source = new Node();
        this.source.x = -200;
        this.source.y = 50;
        this.source.id = "s";
        this.source.color = "#E91E63";

        bpGraph.addNode(this.source);

        //Create sink
        this.sink = new Node();
        this.sink.x = 200;
        this.sink.y = 50;
        this.sink.id = "t";
        this.sink.color = "#4CAF50";

        bpGraph.addNode(this.sink);

        //Create inverse 

        //Link setA to source
        for (let nodeId in bpGraph.setA) {
            //bpGraph.addEdge()
        }

        this.graph = bpGraph;
    }

    private initializeVisitedNodes = () => {

    }


    private getEdgeRemainingCapacity = (edge: IEdge): number => {
        //TODO
        return 0;
    }

    private augmentEdge = (edge: IEdge, bottleNeck: number) => {
        //TODO
        return 0;
    }

    private depthFirstSearch = (node: Node, flow: number): number => {

        if (node === this.sink) return flow;

        this.visitedNodes[node.id] = this.visitedToken;

        let nodeEdges = this.graph.Edges[node.id];
        for (let edge of nodeEdges) {

            let remainingCapacity = this.getEdgeRemainingCapacity(edge);

            if (remainingCapacity > 0 && this.visitedNodes[edge.to.id] != this.visitedToken) {
                let bottleNeck = this.depthFirstSearch(edge.to, Math.min(flow, remainingCapacity));

                if (bottleNeck > 0) {
                    this.augmentEdge(edge, bottleNeck);
                    return bottleNeck;
                }
            }
        }
        return 0;
    }

    dispose = () => {
        this.visitedNodes = null;
    }

}