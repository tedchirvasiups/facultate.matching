﻿const path = require('path');
const devMode = process.env.NODE_ENV !== 'production';
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    entry: {
        app: './src/main.ts'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json']
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: [/\.scss$/, /\.css$/],
                use: [
                    devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
                    "css-loader"
                ]
            },
            {
                test: /\.tsx?$/,
                loader: "ts-loader",
                include: path.resolve(__dirname, "src")
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: ['wwwroot/dist']
        }),
        new HtmlWebpackPlugin({
            chunks: ['app'],
            inject: false, //Generam tag-urile manual prin sintaxa de templating lodash
            template: path.resolve(__dirname, 'Views/Default/Index.template.cshtml'),
            filename: path.resolve(__dirname, 'Views/Default/Index.cshtml')
        }),
        new MiniCssExtractPlugin({
            filename: devMode ? '[name].bundle.[hash].css' : '[name].bundle.[contenthash].css',
            chunkFilename: devMode ? '[id].bundle.css' : '[id].bundle.css'
        })
    ],
    output: {
        filename: devMode ? '[name].bundle.[hash].js' : '[name].bundle.[chunkhash].js',
        path: path.resolve(__dirname, 'wwwroot/dist'),
        publicPath: '/dist/'
    }
};